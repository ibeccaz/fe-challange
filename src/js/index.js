import VueResource from 'vue-resource';
import Vue from 'vue';
import Tweets from '../components/Tweets';
import ThemeControl from '../components/ThemeControl';

Vue.use(VueResource);

new Vue({
  el : '#app',
  components : {
    Tweets,
    ThemeControl
  }
});