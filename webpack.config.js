const webpack = require('webpack');
const path = require('path');
const { VueLoaderPlugin } = require("vue-loader");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = [
{
  mode: "development",
  entry : {
    app: ["./src/scss/styles.scss",          
          "./src/js/index.js"],
    light : "./src/scss/light.scss",
    dark : "./src/scss/dark.scss"
  },
  output: {
      // "path": __dirname+'/dist',
      path : path.join(__dirname, 'dist'),
      filename: "[name].js"
  },
  resolve: {
    modules: [path.resolve(__dirname, "./node_modules")],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: [".js", ".json", ".vue"]
  },
  module: {
      rules: [
          {
              test: /\.vue$/,
              exclude: /node_modules/,
              use: "vue-loader"
          },
          {
              test: /\.scss$/,
              use: [
                  MiniCssExtractPlugin.loader,
                  "css-loader",
                  "sass-loader"
              ]
          }
      ]
  },
  plugins: [
    new MiniCssExtractPlugin({filename: "[name].css"}), 
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      Vue: ["vue/dist/vue.esm.js", "default"]
    }),
  ]
}]